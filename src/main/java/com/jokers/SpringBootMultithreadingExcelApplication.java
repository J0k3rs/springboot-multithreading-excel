package com.jokers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMultithreadingExcelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMultithreadingExcelApplication.class, args);
	}

}
