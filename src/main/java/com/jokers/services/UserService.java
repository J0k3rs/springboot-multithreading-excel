package com.jokers.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.jokers.entities.User;
import com.jokers.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;

	Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Async
	public CompletableFuture<List<User>> saveUsers(MultipartFile file) throws Exception{
		Long start = System.currentTimeMillis();
		List<User> users = parseCSVFile(file);
		logger.info("Saving list of users of size {}", users.size(), ""+Thread.currentThread().getName());
		users = repository.saveAll(users);
		/*
		 * for(User u:users) { System.out.println("User - "+u); }
		 */
		Long end = System.currentTimeMillis();
		logger.info("Total time {}", end-start);
		return CompletableFuture.completedFuture(users);
	}
	
	@Async
	public CompletableFuture<List<User>> findAllUsers(){
		logger.info("Get list of Users by "+Thread.currentThread().getName());
		List<User> users = repository.findAll();
		return CompletableFuture.completedFuture(users);
		
	}
	
	public List<User> parseCSVFile(final MultipartFile file) throws Exception{
		final List<User> users = new ArrayList<>();
		try {
			try (final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))){	
			String line;
			while((line = br.readLine()) != null) {
					final String[] data = line.split(",");
					final User user = new User();
					user.setFirstName(data[0]);
					user.setLastName(data[1]);
					user.setEmail(data[2]);
					user.setGender(data[3]);
					user.setIpAddress(data[4]);
					users.add(user);
				}
			return users;
			}	
		} catch (final IOException e) {
			logger.error("First - Failed to parse CSV file {}", e);
			throw new Exception("Second - Failed to parse CSV file {}", e);
		}
		
	}
}
